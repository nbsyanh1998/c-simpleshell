#include "ownLib.h"


int main(){
    char *line;
    char **args, **argsPipe;
    char* linePipes[2];
    while (1)
    {
        printf("osh>");
        fflush(stdout);
        line = readLine();
        if(strlen(line)!=0){
            if(strcmp(line, "!!") == 0){
                line = history[historyIndex-1];
                printf("%s\n", line);
            }
            else{
                history[historyIndex++]=line;
            }
            int pipe = parsePipe(line, linePipes);
            
            if(pipe){
                args = getArgs(linePipes[0]);
                argsPipe = getArgs(linePipes[1]);
            }
            else{
                args = getArgs(line);
            }
            int flag=handleComand(args);
            if(flag){
                if(pipe){
                    execArgsPiped(args, argsPipe);
                }else{
                    execArgs(args);
                }
            }
        }
        
    }

    // clear history
    for(int i=0;i<HISTORY_SIZE;i++){
        free(history[i]);
        history[i]=NULL;
    }
    return 0;
}

