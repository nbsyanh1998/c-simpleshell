#include "lib.h"

char *readLine(){
    int size = MAX_LINE;
    int index = 0;
    int c;
    char *buffer = malloc(sizeof(char)*size);

    if(!buffer){
        fprintf(stderr, "allocation error\n");
        exit(1);
    }
    while (1)
    {
        c = getchar();
        if(c == EOF || c == '\n'){
            buffer[index] = '\0';
            return buffer;
        } else{
            buffer[index] = c;
        }
        index++;
        if(index>=size){
            size+=MAX_LINE;
            buffer = realloc(buffer, size);
            if(!buffer){
                fprintf(stderr, "allocation error\n");
                exit(1);
            }
        }
    }
    
}
