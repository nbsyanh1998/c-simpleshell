#include "lib.h"



int main(void){
	char *args[100];
	int child_status;
	int shouldRun = 1;

	char *list[]={"ls"};

	while(1){
		printf("osh>");
		fflush(stdout);	

		// get user command
		char *buffer = readLine();
		if(buffer == NULL || strcmp(buffer, "")==0){
			continue;
		}

		// exit program
		if(strcmp(buffer, "exit")==0)
			break;
		
		// get args array
		char *command = strtok(buffer, " ");
		int index=0;
		while(command != NULL){
			args[index++] = command;
			command = strtok(NULL, " ");
		}
		args[index] = NULL;

		//fork a child process
		pid_t newPid = fork();
		switch(newPid){
			case -1: // fork không tạo được tiến trình mới
			printf("Fork error.");
			exit(1);
		case 0:;
			int statusCode = execvp(args[0], args);
			if (statusCode == -1) {
			    printf("Terminated Incorrectly\n");
			    return 1;
			}
			exit(0);
			// Mã lỗi trả về của tiến trình con
		default: // fork thành công, chúng ta đang ở trong tiến trình cha
			// Chờ tiến trình con kết thúc
			wait( &child_status );		
		}
	}
	return 0;
}
