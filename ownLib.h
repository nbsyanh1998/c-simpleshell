#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<string.h>


#define MAX_LINE 80
#define DELIM " \n\t\a\r"
#define HISTORY_SIZE 30
int historyIndex = 0;
char *history[HISTORY_SIZE];

//execute normal command
void execArgs(char** args);

//execute piped command
void execArgsPiped(char** args, char** argsPipe);

//read line from user' input
char *readLine();

// get args array from user' input
char **getArgs(char *line);

// handle command: cd, exit, !!, history
int handleComand(char **args);

// show commands history
void showHistory();

// add command to history
void addHistory(char* line);

// parse user' input to two command
int parsePipe(char* line, char** args);

/* Function implement */


void showHistory(){
    int i=0;
    while (history[i]!=NULL)
    {
        printf("%s\n", history[i]);
        i++;       
    }
    
}
int handleComand(char **args){
    int commandsLen = 3;
    char *commands[commandsLen];
    commands[0]="exit";
    commands[1]="cd";
    commands[2]="history";

    for(int i=0;i<commandsLen;i++){
        if(strcmp(args[0], "") == 0)
            return 0;
        if(strcmp(args[0], commands[i]) == 0){
            switch (i)
            {
            case 0:
                exit(0);
            case 1:
                chdir(args[1]);
                return 0;
            case 2:
                showHistory();
                return 0;               
            default:
                break;
            }
        }
    }
    return 1;
}


char *readLine(){
    int size = MAX_LINE;
    int index = 0;
    int c;
    char *buffer = malloc(sizeof(char)*size);

    if(!buffer){
        fprintf(stderr, "allocation error\n");
        exit(EXIT_FAILURE);
    }
    while (1)
    {
        c = getchar();
        if(c == EOF || c == '\n'){
            buffer[index] = '\0';
            return buffer;
        } else{
            buffer[index] = c;
        }
        index++;
        if(index>=size){
            size+=MAX_LINE;
            buffer = realloc(buffer, size);
            if(!buffer){
                fprintf(stderr, "allocation error\n");
                exit(EXIT_FAILURE);
            }
        }
    }
    
}

char **getArgs(char *line){
    int index = 0;
    int size = MAX_LINE;
    char **tokens = malloc(size * sizeof(char*));
    char *token;
    if(!tokens){
        fprintf(stderr, "allocation error\n");
        exit(EXIT_FAILURE);
    }

    token  = strtok(strdup(line), DELIM);

    while (token != NULL)
    {
        tokens[index] = token;
        index++;
        if(index >= size){
            size += MAX_LINE;
            tokens = realloc(tokens, size * sizeof(char*));
            if(!tokens){
                fprintf(stderr, "allocation error\n");
                exit(EXIT_FAILURE);
            }
        }
        token = strtok(NULL, DELIM);
    }
    tokens[index] = NULL;
    return tokens;
}

int parsePipe(char* line, char** args){ 
    args[0] = strtok(strdup(line), "|"); 
    if(args[0]!=NULL) { 
        args[1] = strtok(NULL, "|"); 
    } 
    return args[1] != NULL;
} 

void execArgs(char** args){ 
    // Forking a child 
    pid_t pid = fork();  
    
    if (pid == -1) { 
        printf("Failed forking child..\n"); 
        return; 
    } else if (pid == 0) { 
        if (execvp(args[0], args) < 0) { 
            printf("Could not execute command..\n"); 
        } 
        exit(0); 
    } else { 
        wait(NULL);  
        return; 
    } 
} 

void execArgsPiped(char** args, char** argsPipe){ 
    // 0 is read end, 1 is write end 
    int pipefd[2];  
    pid_t pid1, pid2; 
  
    if (pipe(pipefd) < 0) { 
        printf("\nPipe could not be initialized"); 
        return; 
    } 
    pid1 = fork(); 
    if (pid1 < 0) { 
        printf("\nCould not fork"); 
        return; 
    } 
  
    if (pid1 == 0) { 
        close(pipefd[0]); 
        dup2(pipefd[1], STDOUT_FILENO); 
        close(pipefd[1]); 
  
        if (execvp(args[0], args) < 0) { 
            printf("\nCould not execute command 1.."); 
            exit(0); 
        } 
    } else { 
        pid2 = fork(); 
        if (pid2 < 0) { 
            printf("\nCould not fork"); 
            return; 
        } 
        if (pid2 == 0) { 
            close(pipefd[1]); 
            dup2(pipefd[0], STDIN_FILENO); 
            close(pipefd[0]); 
            if (execvp(argsPipe[0], argsPipe) < 0) { 
                printf("\nCould not execute command 2.."); 
                exit(0); 
            } 

        } else { 
            wait(NULL); 
            wait(NULL); 
        } 
    } 
} 